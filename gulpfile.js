var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cleanCss = require('gulp-clean-css');
var del = require('del');
var log = require('fancy-log');
var handlebars = require('gulp-compile-handlebars');
var rename = require('gulp-rename');
var browserSync = require("browser-sync").create();


// ===================================== Sass to Css =====================================
gulp.task("sass", function(){
     gulp.src(["src/sass/vendor/**/*"])
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("src/css/vendor/"));

    gulp.src(["src/sass/vue/**/*"])
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("src/css/vue/"));    
     
     return gulp.src(["src/sass/**/*","!src/sass/vue/"])
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("src/css/"));
});


// ===================================== CSS concat and bundle =====================================
gulp.task("css", function(){
    return gulp.src(["src/css/vendor/**/*.css", "src/css/**/*", "!src/css/vue/**/*"])
               .pipe(concat("bundle.css"))
               .pipe(cleanCss())
               .pipe(gulp.dest("dist/css/"));
});


// ===================================== clean Src css Folder =====================================
gulp.task('clean:css', function () {
    return del([
      'src/css/*.css', '!src/css/vendor/', '!src/css/vue/'
    ]);
  });


// ===================================== JS concat and bundle =====================================
gulp.task("js", function(){
  return gulp.src(["src/js/vendor/*.js", "src/js/app.js"])
             .pipe(concat("bundle.js"))
             .pipe(gulp.dest("dist/js/"));
});


// ===================================== clean Src js Folder =====================================
gulp.task('clean:js', function () {
  return del([
    'src/js/**/*',
  ]);
});


// ===================================== Move Vue CSS to Dist =====================================
gulp.task('vue-css', function(){

    return gulp.src(['./src/css/vue/*'])
               .pipe(gulp.dest('dist/css/vue/'));

});


// ===================================== Move Vue Js to Dist =====================================
gulp.task('vue', function(){

    return gulp.src(['./src/js/vue/*'])
               .pipe(gulp.dest('dist/js/vue/'));

});


// ===================================== Move Images to Dist =====================================
gulp.task('img', function(){
    return gulp.src(['./src/img/*'])
               .pipe(gulp.dest('dist/img/'));
});


// ===================================== Move Fonts to Dist =====================================
gulp.task('fonts', function(){

    return gulp.src(['./src/fonts/*'])
               .pipe(gulp.dest('dist/fonts/'));

});


// ===================================== Move APIs to Dist =====================================
gulp.task('api', function(){

    return gulp.src(['./src/api/*'])
               .pipe(gulp.dest('dist/api/'));

});


// ===================================== Handlebars template setup =====================================
gulp.task('html', function(){

    return gulp.src('./src/pages/*.hbs')
    .pipe(handlebars({}, {
      ignorePartials: true,
      batch: ['./src/pages/partials']
    }))
    .pipe(rename({
      extname: '.html'
    }))
    .pipe(gulp.dest('./dist'));

});


// ===================================== Clean Dist folder ===================================== //
gulp.task('clean:dist', function () {
    return del([
      'dist/*',
    ]);
  });


// =================================== Copy Dist Folder to IIS Inet pub local host ======================== //
gulp.task('copy-iis', () => {

  return  gulp.src('dist/**')
          .on('end', function(){ log("Copying Dist folder to IIS"); })
          .pipe( gulp.dest("C:/inetpub/wwwroot/Kurnia-poc") )
          .on('end', function(){ log("files successfully copied"); });

});


// ===================================== Watch file changes and run Build ======================
gulp.task('watch', function(){

    gulp.watch(
        ['./src/pages','./src/sass/','./src/js/','./src/img/','./src/api/'],
        gulp.series('build', reload)
    )

});


// ===================================== Build Dist files =====================================
gulp.task('build', 
    gulp.series(
        'clean:css',
        'clean:dist',
        'sass',
        'css',
        'js',
        'vue',
        'vue-css',
        'img',
        'fonts',
        'api',
        'html'
        ),
);


// ===================================== Reload browser =====================================
function reload(done) {
    browserSync.reload();
    done();
}


// ===================================== serve files using Browser sync =====================================
gulp.task('serve',
    gulp.series('build', serve, 'watch')
);


function serve(done) {
    browserSync.init({
      server: {
        baseDir: './dist/'
      }
    });
    done();
}