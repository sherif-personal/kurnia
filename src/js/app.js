//Global Variables
var apiBaseUrl = "http://api.kurnia.labworksdigital.com/"
var delimiters = ['{[', ']}'];
var screenBreakpoints = {
    "xl": 1140,
    "tab":1024,
    "lg": 960,
    "md": 768,
    "sm": 540,
};

var currentWindowWidth = $(window).width();

console.log("currentWindowWidth: ", currentWindowWidth);



if( $("#homePageModule").length ){
var homePageModule = new Vue({
    delimiters: delimiters,
    el: "#homePageModule",
    data() {
        return {
            moduleTitle: 'Kurnia Application',
        }
    }
});
}


// Events Module
if( $("#eventsModule").length ){
var eventsModule = new Vue({
    delimiters: delimiters,
    el: '#eventsModule',
    data() {
        return {
            moduleTitle: 'Events Management',
            eventGroups: [],
            selectedEventGroup: '',
            selectedEventGroupForm: '',
            eventsTableHeaders: [
                {
                    text: "Event name",
                    value: "Name"
                },
                {
                    text: "Actions",
                    value: "Name",
                    sortable: false,
                    align: "center"
                }
            ],
            eventsTableData: [],
            tableLoading: true,
            saving:false,
            searchTerm: "",
            editAddModal: false,
            addEventGroupModal: false,
            addEventFormFailAlert: false,
            addEventFormSuccessAlert: false,
            addEventGroupFormFailAlert: false,
            addEventGroupFormSuccessAlert: false,
            addEventFormError: "",
            addEventGroupFormError: "",
            formTitle: "",
            errorGroupName: false,
            formEventValid: true,
            formGroupValid: true,
            formProcessing: false,
            nameRules: [
                v => !!v || 'Name is required',
                v => v.length >= 2 || 'Name must be more than 2 characters'
            ],
            editEventDefaultData: {
                Id: "",
                Name: "",
            },
            editedEventData: {
                Id: "",
                Name: "",
            },
            addEventGroupData: {
                Id: "",
                Name: "",
            }
        }
    },
    methods: {
        init: function(){
            this.getEventGroups();
        },
        getEventGroups: function() {

            var self = this;
            var getEventGroupsApi = apiBaseUrl + "event-groups";

            axios.get(getEventGroupsApi)
                 .then(function(response){

                    self.eventGroups = response.data;

                    if(self.addEventGroupData.Id != ""){
                        self.selectedEventGroup = self.addEventGroupData.Id;
                    } else if (self.selectedEventGroupForm != ""){
                        self.selectedEventGroup = self.selectedEventGroupForm;
                    } else {
                        self.selectedEventGroup = self.eventGroups[0].Id;
                    }



                    self.getEvents();

                 });
        },
        getEvents: function() {

            var getEventsApi = apiBaseUrl + "event-groups/"+this.selectedEventGroup+"/events";

            //Show table loading bar
            this.tableLoading = true;

            axios.get(getEventsApi)
                 .then( 
                     response => {

                        //Hide table loading bar
                        this.tableLoading = false;

                        if (response.data.length > 0){
                            this.eventsTableData = response.data;
                        } else {
                            this.eventsTableData = [];
                        }

                     }
                )
        },
        eventGroupSelectChange: function() {
            this.getEvents();
        },
        editItem: function(item){
            /*
            this.formTitle = "Edit Event";
            this.addEventFormFailAlert = false;
            this.editedEventData = Object.assign({}, item);
            this.editAddModal = true;
            */
        },
        saveEdited: function(){
            this.saving = true;
        },
        cancelEdit: function(){
            this.editAddModal = false;
            //this.editedEventData = Object.assign({}, this.editEventDefaultData);
        },
        addEventGroup: function(){
            this.addEventGroupModal = true;
            this.addEventGroupFormFailAlert = false;
            this.$refs.AddEventGroupform.reset();
        },
        cancelAddEventGroup: function(){
            this.addEventGroupModal = false;
        },
        saveEventGroup: function(){

            if (this.$refs.AddEventGroupform.validate()) {
                
                this.formProcessing = true;
                var addEventGroupApi = apiBaseUrl + "event-groups/";

                axios.post(addEventGroupApi,"="+this.addEventGroupData.Name)
                     .then( 
                        response => {
                            console.log(response);
                            this.addEventGroupFormSuccessAlert = true;
                            setTimeout(() => {
                                                this.addEventGroupModal = false;
                                                this.getEventGroups();
                                                this.addEventGroupFormSuccessAlert = false;
                                             }, 1000);
                        }
                     )
                     .catch(
                         error => {
                            console.log(error);
                            this.addEventGroupFormError = error.response.status +" "+ error.response.statusText;
                            this.addEventGroupFormFailAlert = true;
                         }
                     )
                     .then(
                        () => {
                            this.formProcessing = false;
                        }
                     );

            }
        },
        addEvent: function(){
            this.formTitle = "Add Event";
            this.addEventFormFailAlert = false;
            this.editAddModal = true;
            //this.selectedEventGroupForm = this.selectedEventGroup;
            console.log("selectedEventGroupForm: ", this.selectedEventGroupForm);
            this.$refs.AddEditEventform.reset();
        },
        saveEvent: function(){

            if (this.$refs.AddEditEventform.validate()) {
                
                this.formProcessing = true;
                var addEventApi = apiBaseUrl + "event-groups/"+this.selectedEventGroupForm+"/events/";
                //var formData = {Name: this.editedEventData.Name};
                
                axios.post(addEventApi, {Name: this.editedEventData.Name})
                     .then( 
                        response => {
                            console.log(response);
                            this.addEventFormSuccessAlert = true;
                            setTimeout(() => {
                                                this.editAddModal = false;
                                                this.getEventGroups();
                                                this.addEventFormSuccessAlert = false;

                                             }, 1000);
                        }
                     )
                     .catch(
                         error => {
                            console.log(error);
                            this.addEventFormError = error.response.status +" "+ error.response.statusText;
                            this.addEventFormFailAlert = true;
                         }
                     )
                     .then(
                        () => {
                            this.formProcessing = false;
                        }
                     );
                
            }
        }
    },
    mounted() {
        this.init();
    }
});
}



// Recipients Module
if( $("#recipientsModule").length ){
var recipientsModule = new Vue({
    delimiters: delimiters,
    el: '#recipientsModule',
    data() {
        return {
            moduleTitle: 'Recipients Management',
            menuDob: false,
            tableLoading: false,
            formProcessing: false,
            searchTerm: '',
            recipientsTableData: [],
            recipientsTableHeaders: [
                {
                    text: "Name",
                    value: "FullName",
                },
                {
                    text: "IC No.",
                    value: "IdentificationCardNumber",
                },
                {
                    text: "Birth Cert No.",
                    value: "BirthCertificateNumber",
                },
                {
                    text: "Date of Birth",
                    value: "DateOfBirth",
                },
                {
                    text: "Gender",
                    value: "Gender",
                },
                {
                    text: "Phone No.",
                    value: "PhoneNumber",
                },
                {
                    text: "Actions",
                    value: "Name",
                    sortable: false,
                    align: "center"
                },
            ],
            recipientsTableHeadersMobile: [
                {
                    text: "Name",
                    value: "FullName",
                },
            ],
            editAddRecipientModal: false,
            deleteRecipientModal: false,
            nameRules: [
                v => !!v || 'Name is required',
                v => v.length >= 2 || 'Name must be more than 2 characters'
            ],
            IcRules: [
                v => !!v || 'IC No. is required',
                v => v.length >= 3 || 'IC must be more than 3 characters'
            ],
            BcertRules: [
                v => !!v || 'Birth Cert No. is required',
                v => v.length >= 3 || 'Cert No. must be more than 3 characters'
            ],
            PhoneRules: [
                v => !!v || 'Phone No. is required',
                v => v.length >= 3 || 'Phone No. must be more than 3 characters'
            ],
            formTitle: "",
            formRecipientValid: false,
            formDeleteRecipientValid: true,
            formDatePickerMenu: false,
            formDobMinDate: new Date().toISOString().substr(0, 10),
            formEditFlag: false,
            addRecipientFormError: "",
            deleteRecipientFormError: "",
            addRecipientFormFailAlert: false,
            addRecipientFormSuccessAlert: false,
            DeleteRecipientFormFailAlert: false,
            DeleteRecipientFormSuccessAlert: false,
            recipientData: {
                FullName: "",
                IdentificationCardNumber: "",
                BirthCertificateNumber: "",
                DateOfBirth: "",
                Gender: "",
                PhoneNumber: ""
            },
            recipientToDeleteData: {
                Id: "",
                FullName: "",
            }
        }
    },
    watch: {
        menuDob (val) {
          val && this.$nextTick(() => (this.$refs.picker.activePicker = 'YEAR'))
        }
      },
    computed: {
        computedYongestDob () {
            return moment().subtract(18, "years").format('YYYY-MM-DD');
        },
        computedOldestDob () {
            return moment().subtract(99, "years").format('YYYY-MM-DD');
        }
    },
    methods: {
        init: function(){
            this.getrecipients();
        },
        getrecipients: function(){

            var getRecipientsApi = apiBaseUrl + "recipients";

            //Show table loading bar
            this.tableLoading = true;

            axios.get(getRecipientsApi)
                 .then( 
                     response => {

                        //Hide table loading bar
                        this.tableLoading = false;

                        var tempData = response.data;

                        tempData.forEach(function(recipient){
                            var recipientDob =  moment(recipient.DateOfBirth, "YYYY-MM-DDTHH:mm:ss").format("DD-MM-YYYY");
                            recipient.DateOfBirth = recipientDob;
                        });

                        this.recipientsTableData = tempData;
                        
                     }
                );

        },
        addRecipient: function(){
            this.formEditFlag = false;
            this.formTitle = "Add Recipient";
            this.addRecipientFormFailAlert = false;
            this.addRecipientFormSuccessAlert = false;
            this.editAddRecipientModal = true;
            this.$refs.AddEditRecipientform.reset();
        },
        editRecipiant: function(item){
            this.formEditFlag = true;
            this.formTitle = "Edit Recipient";
            this.addRecipientFormFailAlert = false;
            this.addRecipientFormSuccessAlert = false;
            this.recipientData = Object.assign({}, item);
            this.editAddRecipientModal = true;
        },
        saveRecipient: function(){

            if (this.$refs.AddEditRecipientform.validate()) {

                this.formProcessing = true;
                var addRecipientApi = apiBaseUrl + "recipients";

                //console.log(this.recipientData.DateOfBirth);
                
                
                axios.post(addRecipientApi, this.recipientData)
                     .then( 
                        response => {
                            console.log(response);
                            this.addRecipientFormSuccessAlert = true;
                            setTimeout(() => {
                                                this.editAddRecipientModal = false;
                                                this.getrecipients();

                                             }, 1000);
                        }
                     )
                     .catch(
                         error => {
                            console.log(error);
                            this.addRecipientFormError = error.response.status +" "+ error.response.statusText;
                            this.addRecipientFormFailAlert = true;
                         }
                     )
                     .then(
                        () => {
                            this.formProcessing = false;
                        }
                     );
            
            }

        },
        saveEditedRecipient: function(){    

            if (this.$refs.AddEditRecipientform.validate()) {

                this.formProcessing = true;
                var editRecipientApi = apiBaseUrl + "recipients/"+this.recipientData.Id;

                axios.patch(editRecipientApi, this.recipientData)
                     .then( 
                        response => {
                            console.log(response);
                            this.addRecipientFormSuccessAlert = true;
                            setTimeout(() => {
                                                this.editAddRecipientModal = false;
                                                this.getrecipients();

                                             }, 1000);
                        }
                     )
                     .catch(
                         error => {
                            console.log(error);
                            this.addRecipientFormError = error.response.status +" "+ error.response.statusText;
                            this.addRecipientFormFailAlert = true;
                         }
                     )
                     .then(
                        () => {
                            this.formProcessing = false;
                        }
                     );

            }
        },
        cancelEdit: function(){
            this.editAddRecipientModal = false;
            this.$refs.AddEditRecipientform.reset();
        },
        deleteRecipient: function(item){
            this.deleteRecipientModal = true;
            this.recipientToDeleteData.Id = item.Id;
            this.recipientToDeleteData.FullName = item.FullName;
        },
        saveDeleteRecipient: function(){

            this.formProcessing = true;
            var deleteRecipientApi = apiBaseUrl + "recipients/"+this.recipientToDeleteData.Id;

            axios.delete(deleteRecipientApi, {})
                     .then( 
                        response => {
                            console.log(response);
                            this.DeleteRecipientFormSuccessAlert = true;
                            setTimeout(() => {
                                                this.deleteRecipientModal = false;
                                                this.getrecipients();

                                             }, 1000);
                        }
                     )
                     .catch(
                         error => {
                            console.log(error);
                            this.deleteRecipientFormError = error.response.status +" "+ error.response.statusText;
                            this.DeleteRecipientFormFailAlert = true;
                         }
                     )
                     .then(
                        () => {
                            this.formProcessing = false;
                        }
                     );


        },
        cancelDelete: function(){
            this.deleteRecipientModal = false;
            this.$refs.DeleteRecipientform.reset();
        }
    },
    mounted(){
        this.init();
    }
});
}



// Registrants Module
if( $("#registrantsModule").length ){
var registrantsModule = new Vue({
    delimiters: delimiters,
    el: '#registrantsModule',
    data(){
        return {
            moduleTitle: 'Registrants Management',
            eventGroups: [],
            selectedEventGroup: '',
            events: [],
            selectedEvent: "",
            tableLoading: true,
            saving:false,
            formProcessing: false,
            searchTermRegistrants: "",
            searchTermRecipiants: "",
            registrantsTableData: [],
            registrantsTableHeaders: [
                {
                    text: "Name",
                    value: "FullName",
                },
                {
                    text: "IC No.",
                    value: "IdentificationCardNumber",
                },
                {
                    text: "Birth Cert No.",
                    value: "BirthCertificateNumber",
                },
                {
                    text: "Date of Birth",
                    value: "DateOfBirth",
                },
                {
                    text: "Gender",
                    value: "Gender",
                },
                {
                    text: "Phone No.",
                    value: "PhoneNumber",
                },
                {
                    text: "Actions",
                    value: "Name",
                    sortable: false,
                    align: "center"
                },
            ],
            registrantsTableHeadersMobile: [
                {
                    text: "Name",
                    value: "FullName",
                    align: "left"
                },
            ],
            recipientsTableData: [],
            recipientsTableHeaders: [
                {
                    text: "Name",
                    value: "FullName",
                },
                {
                    text: "IC No.",
                    value: "IdentificationCardNumber",
                },
                {
                    text: "Birth Cert No.",
                    value: "BirthCertificateNumber",
                },
                {
                    text: "Date of Birth",
                    value: "DateOfBirth",
                },
                {
                    text: "Gender",
                    value: "Gender",
                },
                {
                    text: "Phone No.",
                    value: "PhoneNumber",
                },
            ],
            addRegistrantsToEventModal: false,
            removeRegistranttModal: false,
            passDetailsModal: false,
            addPassModal: false,
            processingGiftModal: false,
            qrGiftModal: false,
            selectedRecipients: [],
            selectedRegistrants: [],
            RecipientNotSelected: true,
            registrantsIds: [],
            registrantsRemoveIds: [],
            registrantToDeleteData: {
                Id: "",
                FullName: "",
            },
            deleteRegistrantFormError: "",
            deleteRegistrantFormFailAlert: "",
            deleteRegistrantFormSuccessAlert: "",
            eventsInGroup: true,
            pass: {
                RegistrantId: "",
                FullName: "",
                details: {},
            },
            isPassPrinted: false,
            registrantsStats: {

            },
            statsAvailable: false,
            registrantQueryUrlId: "",
            registrantFromQueryUrlData: {},
            registrantFromQueryUrlGiftRecived: false,
            queryParamRegId: false,
        }
    },
    computed: {
        registrantsSelected(){
            if( this.selectedRegistrants.length > 0 ){
                return true;
            } else {
                return false;
            }
        }
    },
    methods: {
        init: function(){

            this.getEventGroups();
            this.checkQueryUrl();        
            
        },
        getEventGroups: function() {

            var getEventGroupsApi = apiBaseUrl + "event-groups";

            axios.get(getEventGroupsApi)
                 .then( 
                     response => (
                                    this.eventGroups = response.data,
                                    this.selectedEventGroup = this.eventGroups[0].Id,
                                    this.getEvents()
                                )
                )
        },
        getEvents: function() {

            var getEventsApi = apiBaseUrl + "event-groups/"+this.selectedEventGroup+"/events";

            console.log("selected Group:", this.selectedEventGroup);

            axios.get(getEventsApi)
                 .then( 
                     response => {

                        if(response.data.length > 0){
                            this.events = response.data;
                            
                            
                            
                            if(this.registrantQueryUrlId){
                                console.log("change event", this.registrantFromQueryUrlData.EventGrouping.EventId);
                                console.log("events ", JSON.stringify(this.events)) ;
                                this.selectedEvent = this.registrantFromQueryUrlData.EventGrouping.EventId;
                            } else {
                                this.selectedEvent = this.events[0].Id;
                            }
                            
                            this.getRegistrants();
                            this.eventsInGroup = true;
                            this.statsAvailable = true; 
                        } else {
                            this.events = [];
                            this.selectedEvent = "";
                            this.registrantsTableData = [];
                            this.eventsInGroup = false;
                            this.statsAvailable = false;
                        }

                     }
                )
        },
        eventGroupSelectChange: function() {
            this.getEvents();
        },
        eventSelectChange: function() {
            this.getRegistrants();
        },
        getRegistrants: function(){

            var getRegistrantsApi = apiBaseUrl + "events/"+this.selectedEvent+"/registrants";

            //Show table loading bar
            this.tableLoading = true;

            axios.get(getRegistrantsApi)
                 .then( 
                     response => {
                        console.log("Registrants: ",response.data);

                        //Hide table loading bar
                        this.tableLoading = false;

                        if (response.data.length > 0){
                            
                            //this.registrantsTableData = response.data;

                            var tempData = response.data;

                            tempData.forEach(function(recipient){

                                var recipientDob =  moment(recipient.DateOfBirth, "YYYY-MM-DDTHH:mm:ss").format("DD-MM-YYYY");
                                recipient.DateOfBirth = recipientDob;

                                if(recipient.Gift.DateReceived!=null){
                                    var giftRecDate = moment(recipient.Gift.DateReceived, "YYYY-MM-DDTHH:mm:ss").format("DD-MM-YYYY|HH:mm:ss");
                                    
                                    var giftRecDateArr = giftRecDate.split("|");
                                    var giftRecDateOnly = giftRecDateArr[0];
                                    var giftRecDateTime = giftRecDateArr[1];
                                    
                                    recipient.Gift.DateReceived = giftRecDateOnly;
                                    recipient.Gift.DateReceivedTime = giftRecDateTime;
                                }

                                console.log(recipient.Id);

                            });

                            this.registrantsTableData = tempData;

                        } else {
                            this.registrantsTableData = [];
                        }

                        this.getReportsData();

                     }
                )
        },
        getAllRecipients: function() {
            this.addRegistrantsToEventModal = true;

            var getRecipientsApi = apiBaseUrl + "recipients";

            //Show table loading bar
            this.tableLoading = true;

            axios.get(getRecipientsApi)
                 .then( 
                     response => {

                        //Hide table loading bar
                        this.tableLoading = false;

                        var tempData = response.data;

                        tempData.forEach(function(recipient){
                            var recipientDob =  moment(recipient.DateOfBirth, "YYYY-MM-DDTHH:mm:ss").format("DD-MM-YYYY");
                            recipient.DateOfBirth = recipientDob;
                        });

                        this.recipientsTableData = tempData;
                        
                     }
                );
            
              

        },
        cancelAddRegistrant: function(){
            this.addRegistrantsToEventModal = false;
        },
        saveAddedRecipientsToEvent: function(){
            
            var recipientsList = [];

            this.selectedRecipients.forEach(function(recipient, index){
                recipientsList.push(recipient.Id)
            });

            this.registrantsIds = recipientsList;

            this.formProcessing = true;

            var addRegistrantsApi = apiBaseUrl + "events/"+this.selectedEvent+"/registrants";

            axios.post(addRegistrantsApi, {RegistrantIdList: this.registrantsIds})
                 .then( 
                    response => {
                        console.log(response);

                        setTimeout(() => {  
                                            this.addRegistrantsToEventModal = false;
                                            this.getRegistrants();
                                            this.selectedRecipients = [];
                                         }, 1000);
                    }
                 )
                 .catch(
                     error => {
                        console.log(error);
                     }
                 )
                 .then(
                    () => {
                        this.formProcessing = false;
                    }
                 );
            
        },
        removeRegistrant: function(registrant){
            //console.log(registrant.Registrant.Id);
            this.removeRegistranttModal = true;
            this.registrantToDeleteData.Id = registrant.Id;
            this.registrantToDeleteData.FullName = registrant.FullName;
        },
        saveRemovedRegistrant: function(){

            this.formProcessing = true;
            var registrantsList = [];
            var removeRegistrantApi = apiBaseUrl + "registrants";

            console.log(this.selectedRegistrants);
            console.log(this.registrantToDeleteData.Id);

            if(this.registrantToDeleteData.Id != ""){
                registrantsList.push(this.registrantToDeleteData.Id);
            } else {
                this.selectedRegistrants.forEach(function(registrant, index){
                    registrantsList.push(registrant.Id)
                });
            }

            console.log(registrantsList);
            
            axios.delete(removeRegistrantApi, {data: {RegistrantIdList: registrantsList}})
                     .then( 
                        response => {
                            console.log(response);
                            this.deleteRegistrantFormSuccessAlert = true;
                            setTimeout(() => {
                                                this.removeRegistranttModal = false;
                                                this.getRegistrants();
                                                this.deleteRegistrantFormSuccessAlert = false;
                                                this.selectedRegistrants = [];
                                             }, 1000);
                        }
                     )
                     .catch(
                         error => {
                            console.log(error);
                            this.deleteRegistrantFormError = error.response.status +" "+ error.response.statusText;
                            this.deleteRegistrantFormFailAlert = true;
                         }
                     )
                     .then(
                        () => {
                            this.formProcessing = false;
                        }
                     );
            
        },
        cancelRemove: function(){
            this.removeRegistranttModal = false;
            this.registrantToDeleteData.Id = "";
            this.registrantToDeleteData.FullName = "";
        },
        createRegistrantPass(registrant){
            
            var createRegistrantPassApi = apiBaseUrl + "registrants/"+registrant.Id+"/passes";
            
            this.addPassModal = true;

            console.log("registrant: ", JSON.stringify( registrant ) );

            
            axios.post(createRegistrantPassApi, {})
                 .then( 
                    response => {
                        
                        console.log("response: ", JSON.stringify( response ) );

                        setTimeout(() => {
                                            this.getRegistrants();
                                            this.pass.FullName = registrant.FullName;
                                            this.pass.details.Id = response.data.Id;
                                            this.addPassModal = false;
                                            this.passDetailsModal = true;

                                         }, 1000);
                    }
                 )
                 .catch(
                     error => {
                        console.log(error);
                     }
                 )
                 .then(
                    () => {
                        //this.addPassModal = false;
                        //this.passDetailsModal = true;
                        //console.log("Pass: ", JSON.stringify(this.pass) );
                    }
                 );
            
            

        },
        printPass: function(registrant){


            console.log(registrant);

            this.pass.FullName = registrant.FullName;
            this.pass.details = registrant.Pass;

            this.passDetailsModal = true;
        },
        setPassPrinted: function(){
            
            if(this.pass.details.Id != null ){

                var setPassPrintedApi = apiBaseUrl + "passes/"+this.pass.details.Id+"/print-validations";
                this.formProcessing = true;

                axios.patch(setPassPrintedApi, {})
                     .then( 
                        response => {
                            console.log(response);

                            setTimeout(() => {
                                    this.formProcessing = false;
                                    this.passDetailsModal = false;
                                    this.getRegistrants();
                                             }, 1000);
                        }
                     )
                     .catch(
                         error => {
                            console.log(error);
                         }
                     )
                     .then(
                        () => {
                            
                        }
                     );

            }

        },
        setGiftReceived: function(registrant){

            var setGiftReceivedApi = apiBaseUrl + "registrants/"+registrant.Id+"/gift-receives";
            
            if(this.registrantQueryUrlId!=""){
                this.qrGiftModal = false;
                this.queryParamRegId = false;
            }
            
            this.processingGiftModal = true;

                axios.patch(setGiftReceivedApi, {})
                     .then( 
                        response => {
                            console.log(response);

                            setTimeout(() => {
                                    this.getRegistrants();
                                    this.processingGiftModal = false;


                                    if(this.registrantQueryUrlId!=""){
                                        this.registrantFromQueryUrlGiftRecived = true;
                                        this.queryParamRegId = true;
                                        this.qrGiftModal = true;
                                    }

                                             }, 1000);
                        }
                     )
                     .catch(
                         error => {
                            console.log(error);
                         }
                     )
                     .then(
                        () => {
                            //this.processingGiftModal = false;
                        }
                     );

        },
        getReportsData: function(){

            var getReportsApi = apiBaseUrl + "events/"+this.selectedEvent+"/reports";

            axios.get(getReportsApi)
                 .then( 
                     response => {
                        console.log("Reports: ",response.data);
                        this.registrantsStats = response.data;
                     }
                )
                 
        },
        goToPassPage: function(pass){

            //console.log(pass.details.Id);

            var pageUrl = "pass.html#"+pass.details.Id;

            window.open(pageUrl, "_blank");

        },
        checkQueryUrl: function(){

            var queyParams = getQueryUrlParams();

            if(queyParams.regId){
                this.registrantQueryUrlId = queyParams.regId;
                this.getRegistrantDetails();
                
            }

        },
        getRegistrantDetails: function(){

            var getRegistrantApi = apiBaseUrl + "registrants/"+this.registrantQueryUrlId;

            axios.get(getRegistrantApi)
                 .then( 
                     response => {
                        this.registrantFromQueryUrlData = response.data;

                        this.registrantFromQueryUrlData.Recipient.DateOfBirth = formatDate(this.registrantFromQueryUrlData.Recipient.DateOfBirth);
                        this.registrantFromQueryUrlGiftRecived = this.registrantFromQueryUrlData.Registrant.IsGiftReceived;
                        this.queryParamRegId = true;
                        this.qrGiftModal = true;

                        this.selectedEventGroup = this.registrantFromQueryUrlData.EventGrouping.GroupId;


                        console.log("Registrant Details: ", JSON.stringify(this.registrantFromQueryUrlData));
                     }
                )

        },
        showPass: function(registrant) {

            //this.registrantQueryUrlId = registrant.Id;
            //this.getRegistrantDetails();
            var passId = registrant.Pass.Id;
            var pass = {};

            pass.details = {
                "Id": passId
            };

            console.log(JSON.stringify(registrant));

            this.goToPassPage(pass);
            

        }

    },
    watch: {
        selectedRecipients(val) {
            if(val.length > 0){
                this.RecipientNotSelected = false;
            } else {
                this.RecipientNotSelected = true;
            }
        }
    },
    mounted() {
        this.init();
    }
});
}


Vue.component('doughnut-chart', {
    extends: VueChartJs.Doughnut,
    props:['chart-data', 'chart-label'],  
    mounted () {

        console.log(this.chartLabel);

        this.renderChart(
            {
                datasets: [{
					data:this.chartData.data,
					backgroundColor: [
                        '#337ab7',
                        '#5cb85c'
					],
					label: 'Dataset 1'
				}],
				labels: this.chartLabel
            }, 
            {
                responsive: true,
				legend: {
					position: 'top',
				},
				title: {
					display: false,
					text: 'Registrans vs Passes Printed'
				},
				animation: {
					animateScale: true,
					animateRotate: true
				}
            }
        )
    }
    
});


// Reports Module
if( $("#reportsModule").length ){
var reportsModule = new Vue({
    delimiters: delimiters,
    el: '#reportsModule',
    data(){
        return {
            moduleTitle: 'Reports',
            eventGroups: [],
            selectedEventGroup: '',
            events: [],
            selectedEvent: "",
            eventsInGroup: true,
            totalRegistrants: "",
            regPassChartData: {
                data: []
            },
            regGiftChartData: {
                data: []
            },
            loaded: false,
            reportsData: {

            },
        }
    },
    methods: {
        init: function(){
            this.getEventGroups();
        },
        getEventGroups: function() {

            var getEventGroupsApi = apiBaseUrl + "event-groups";

            axios.get(getEventGroupsApi)
                 .then( 
                     response => (
                                    this.eventGroups = response.data,
                                    this.selectedEventGroup = this.eventGroups[0].Id,
                                    this.getEvents()
                                )
                )
        },
        getEvents: function() {

            var getEventsApi = apiBaseUrl + "event-groups/"+this.selectedEventGroup+"/events";

            this.loaded = false;

            axios.get(getEventsApi)
                 .then( 
                     response => {

                        if(response.data.length > 0){
                            this.events = response.data;
                            this.selectedEvent = this.events[0].Id;
                            this.getReportsData();
                            this.getRegistrants();
                            this.eventsInGroup = true;
                        } else {
                            this.events = [];
                            this.selectedEvent = "";
                            this.eventsInGroup = false;
                            this.loaded = false;
                        }

                     }
                )
        },
        eventGroupSelectChange: function() {
            this.getEvents();
        },
        eventSelectChange: function() {
            this.getRegistrants();
            this.getReportsData();
        },
        getRegistrants: function(){

            var getRegistrantsApi = apiBaseUrl + "events/"+this.selectedEvent+"/registrants";

            axios.get(getRegistrantsApi)
                 .then( 
                     response => {
                        console.log("Registrants: ",response.data);
                        this.totalRegistrants = response.data.length;
                     }
                )
        },
        getReportsData: function(){

            var getReportsApi = apiBaseUrl + "events/"+this.selectedEvent+"/reports";
            this.loaded = false;

            axios.get(getReportsApi)
                 .then( 
                     response => {
                        console.log("Reports: ",response.data);
                        this.reportsData = response.data;

                        var notPrinted = parseInt(response.data.TotalRegistrants) - parseInt(response.data.TotalPassesPrinted);
                        var notGift = parseInt(response.data.TotalRegistrants) - parseInt(response.data.TotalGiftsReceived);


                        this.regPassChartData.data = [];
                        this.regPassChartData.data.push(response.data.TotalPassesPrinted);
                        this.regPassChartData.data.push(notPrinted);
                        
                        
                        this.regGiftChartData.data = [];
                        this.regGiftChartData.data.push(response.data.TotalGiftsReceived);
                        this.regGiftChartData.data.push(notGift);
                        
                        
                        this.loaded = true;
                     }
                )
                 
        }
    },
    mounted() {
        this.init();
    }
});
}


// Pass Module
if( $("#passModule").length ){
var passModule = new Vue({
    delimiters: delimiters,
    el: "#passModule",
    data(){
        return{
            moduleTitle: "Pass Page",
            passId: "",
            passDetails: {

            },
            registrantId: "",
        }
    },
    methods: {
        init: function(){
            this.getPassId();
        },
        getPassId: function(){

            this.passId = (window.location.hash).substring(1);
            this.getPassDetails();

        },
        getPassDetails: function(){

            var getPassApi = apiBaseUrl + "passes/"+this.passId+"/recipients";

            axios.get(getPassApi)
                 .then( 
                     response => {

                        console.log("Pass: ",response.data);
                        this.passDetails = response.data.Recipient;

                        this.passDetails.DateOfBirth = moment(this.passDetails.DateOfBirth, "YYYY-MM-DDTHH:mm:ss").format("DD-MM-YYYY");
                        this.registrantId = response.data.Registrant.Id;
                     }
                )

        }
    },
    mounted() {
        this.init();
    }
});
}



//Function to get the query params from a URL
function getQueryUrlParams () {

    location.queryString = {};

    location.search.substr(1).split("&").forEach(function (pair) {
        if (pair === "") return;
        var parts = pair.split("=");
        location.queryString[parts[0]] = parts[1] && decodeURIComponent(parts[1].replace(/\+/g, " "));
    });

    return location.queryString;

}


//Format date function 
function formatDate(rawDate){
    return moment(rawDate, "YYYY-MM-DDTHH:mm:ss").format("DD-MM-YYYY");
}